---
comments: false
paginate: false
showdate: false
#draft : true
---
<div id="special">
    About this section:
</div>

<div id="specialmain">
    You will find here information about my academic research, including academic papers, my PhD thesis and my two master's dissertations.
    <p>
    My PhD thesis, conducted between 2015 and 2020 at Oxford and the Australian National Univesrity, draws on a case study of entrepreneurship in contemporary rural China to address questions relating to social mobility, social differentiation, perceptions of social fairness and legitimacy, and the formation of collective identities in a context of rapid socio-economic change.
    <p>
    My first masters' thesis (2011), published by the Parisian publishing house L’Harmattan (see <a class="article-more-link" href="https://www.editions-harmattan.fr/index.asp?navig=catalogue&obj=livre&no=40814&razSqlClone=1">here</a>), analysed how French observers used their own intellectual framework to make sense of the Chinese Cultural Revolution that was taking place before their eyes.
    <p>
    My second masters’ thesis (2013) analysed how different groups of people (migrants workers, migrant bosses, and locals) perceived each other, and how a sense of “otherness” arose from shared life in a Beijing suburb.  

</div>

<div id="special2">
    All posts:
</div>

<p>
    <div id="maclasse">
        <a href="/page/research/poverty-alleviation-article">Poverty Alleviation in China: The Rise of State-Sponsored Corporate Paternalism</a>
    </div>
    <div id="row-image">
        <div id="container1">
            <img src="/taiqian/fupin.jpg" width="20%" align="left">
        <div id="container2">
            <p>My recent article about poverty alleviation policies in China, published by China Perspectives, explores how poverty alleviation programs shape the distribution of power and resources in rural China. <a class="article-more-link" href="/page/research/poverty-alleviation-article">Read More</a>
            </p>
        </div>
    </div>

<p>
    <div id="maclasse">
        <a href="/page/research/phddissertation">PhD dissertation: The self-made entrepreneur in rural China</a>
    </div>
    <div id="row-image">
        <div id="container1">
            <img src="/taiqian/Taiqian4bis.png" width="15%" align="left">
        <div id="container2">
            <p>My PhD thesis draws on a case study of entrepreneurship in contemporary rural China to address questions relating to social mobility, social differentiation, perceptions of social fairness and legitimacy, and the formation of collective identities in a context of rapid socio-economic change. <a class="article-more-link" href="/page/research/phddissertation">Read More</a>
            </p>
        </div>
    </div>

<p>

<div id="maclasse">
    <a href="/page/research/beijing_suburbs">Masters' dissertation: Beijing Suburbs in the Move</a>
</div>
<div id="row-image">
    <div id="container1">
        <img src="/picun14.jpg" width="20%" align="left">
    <div id="container2">
        <p>My master's thesis, based on a research conducted in 2012-2013, analyses the appropriation of residential space in places of cohabitation between local populations and rural migrants. <a class="article-more-link" href="/page/research/beijing_suburbs">Read More</a>
        </p>
    </div>
</div>

<p>
    <div id="maclasse">
        <a href="/page/research/cultural_revolution_under_french_eyes">Masters' dissertation: Cultural revolution under French eyes</a>
    </div>
    <div id="row-image">
        <div id="container1">
            <img src="/cultural_revolution.jpg" width="20%" align="left">
        <div id="container2">
            <p>My master's thesis, based on a research conducted in 2010-2011, investigates China's Cultural Revolution under the French Eyes, especially Diplomats and Travellers in China from 1965 to 1971. <a class="article-more-link" href="/page/research/cultural_revolution_under_french_eyes">Read More</a>
                </p>
            </div>
        </div>
