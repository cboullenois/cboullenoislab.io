---
comments: false
paginate: false
showdate: false
---
<div id="special">About me </div>
<div class="row-img">
  <div class="container-img">
    <img class="left-img" src="/camille.jpg" width="20%" />
    <div id="about_me">
      <p>
        I am a sociologist and China expert trained at Sciences Po, Oxford, and the ANU.
      </p>
      <p>
        I am currently working as a consultant at <a class="article-more-link" href="https://www.sinolytics.de">Sinolytics</a>, where I advise clients on the challenges arising from the Corporate Social Credit System, cybersecurity regulations, and more.
      </p>
      <p>
        Concurrently with academic research, I have worked as a contributor and policy analyst for several research institutes, including <a class="article-more-link" href="http://www.ecfr.eu/publications/C235">China Analysis</a>, the Chinese Academy of Social Sciences, and <a href="https://policycn.com/">China Policy</a>.
        I regularly contribute to <a class="article-more-link" href="https://www.oxan.com/">Oxford Analytica</a> and the <a class="article-more-link" href="http://www.eiu.com/home.aspx">Economist Intelligence Unit</a> on projects pertaining to Chinese politics and society.
      </p>
    </div>
    <div style="clear: both"></div>
  </div>
</div>

<div id="special">My research</div>
<div id="about_me">
  <p>
    I am writing a Ph.D. thesis on social mobility and entrepreneurship in the Chinese countryside.
    My case study focuses on entrepreneurs in Henan.
    It seeks to examine who could become an entrepreneur and how at different stages since the 1980s. It also analyses the consequences of social mobility in terms of personal experience, sense of identity and social relationships.
  </p>
  <p>
    Before my PhD, I completed a master's degree at Inalco in Chinese studies, with a thesis on migrant workers in Beijing suburbs. I conducted a fascinating six-month fieldwork in a migrant village there. Find more
    <a class="article-more-link" href="/page/research/beijing_suburbs">here</a>.
  </p>
  <p>
    Prior to that, I completed a master's degree at Sciences Po (Paris) in history.
    I conducted research on French diplomats and travellers in China during the cultural revolution.
    You can find more about this <a class="article-more-link" href="/page/research/cultural_revolution_under_french_eyes">here</a>.
  </p>
</div>

<div id="special">Contact</div>
  <div id="about_me">
    <p>
      You can email me at 𝛂.𝛃@gmail.com where 𝛂 is my first and 𝛃 my last name.
    </p>
</div>
