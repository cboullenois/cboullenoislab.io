---
title: L’assouplissement de la politique de l’enfant unique
date: 2014-04-15
comments: false
paginate: true
showdate: true
tags: ["one child policy", "demography"]
categories: ["francais"]
description: "Le 16 novembre 2013, l’agence Xinhua annonçait que la décision avait été prise, lors du IIIe Plénum du XVIIIe Congrès du PCC, d'assouplir la politique de l'enfant unique, mise en place à la fin des années 1970. "
featuredImage : "/dusheng2.jpg"

---
<img src="/dusheng2.jpg" width="50%"/>
<div id="légende">"Mankind must control itself and propagate in a planned way".</div>

> Le 16 novembre 2013, l’agence Xinhua annonçait que la décision avait été prise, lors du IIIe Plénum du XVIIIe Congrès du PCC, d'assouplir la « politique de l'enfant unique » (独生子女政策, dusheng zinü zhengce), mise en place à la fin des années 1970.

### Sources
+ Wang Feng<sup>[1](#myfootnote1)</sup>, « Planning familial : une fin longue et difficile », Aisixiang, 23 novembre 2013.
+ Liang Jianzhang<sup>[2](#myfootnote2)</sup> et Huang Wenzheng<sup>[3](#myfootnote3)</sup>, « Que signifie la politique des “deux enfants pour un parent enfant unique” pour l’économie chinoise ? », Aisixiang, 19 novembre 2013.
+ Yi Fuxian<sup>[4](#myfootnote4)</sup>, « À quoi bon passer par l’étape du deuxième enfant ? », Zhongguo gaige wang - China Reform, 1er octobre 2013.
+ Shu Taifeng<sup>[5](#myfootnote5)</sup> et Cao Jinwen<sup>[6](#myfootnote6)</sup>, « Rupture avec la “politique de l’enfant unique”», Caijing, 1er décembre 2013.

Auparavant, plusieurs aménagements apportés à cette politique avaient déjà permis aux ruraux parents d’un premier enfant de sexe féminin, ainsi qu’aux couples dont les deux membres sont enfants uniques, d’avoir deux enfants.

Cette nouvelle réforme, dite des « deux enfants pour un parent enfant unique » (单独两孩, dandu lianghai) et qui permet aux couples dont l’un des membres est enfant unique d’avoir un second enfant, est accueillie par les auteurs sélectionnés ici comme une évolution nécessaire, mais encore insuffisante. Selon Wang Feng, professeur à l’université de Californie et directeur du centre Brookings-Tsinghua, cette réforme vient tard et reste très limitée. Pour Liang Jianzhang et Huang Wenzheng, il faut espérer que c’est le point de départ d’un virage à 180° de la politique démographique chinoise.

#### Une réforme tardive et limitée
Pour Shu Taifeng et Cao Jinwen, cet « ajustement prudent » (最稳妥的调整, zui wentuo de tiaozheng) a suscité en amont de nombreuses résistances, qui expliquent sa portée limitée et la lenteur de sa mise en oeuvre. Les auteurs notent en effet que la question d’un assouplissement de la politique de l’enfant unique a été soulevée dès 2010, lors de la conférence nationale sur la planification familiale. À l’époque, plusieurs propositions destinées à assouplir la politique de l’enfant unique avaient été avancées, parmi lesquelles l’autorisation d’avoir un deuxième enfant limitée à certaines régions, ou encore l’autorisation d’avoir un deuxième enfant après 35 ans, ce qui revenait à un espacement forcé des naissances. Cependant, de nombreux dirigeants s’y étaient à l’époque opposés, tandis que d’autres proposaient même de renforcer le contrôle des naissances. La mise en place de cette réforme avait été programmée en trois étapes : tout d’abord, une expérimentation dans cinq provinces pilotes en 2011, puis dans six autres zones comprenant Pékin, Shanghai et Tianjin, enfin une mise en place globale à l’échelle du pays avant la fin du XIIe Plan quinquennal, en 2015.

Mais, le 16 novembre 2013, le responsable de la Commission nationale pour la santé et la planification familiale, interviewé par l’agence de presse Xinhua<sup>[7](#myfootnote7)</sup>, a indiqué que la nouvelle politique devra faire l’objet d’amendements lors du Congrès provincial et que l’agenda en serait remis à la responsabilité de chaque province. D’après l’article de Caijing, les responsables de la Commission pour Pékin ont déjà indiqué que la réforme ne sera pas mise en place dans la capitale avant la révision du « règlement sur la population et le planning familial à Pékin » (北京市人口与计划 生育条例, Beijingshi renkou yu jihua shengyu tiaoli) – qui régit les questions de populations à Pékin – en 2014.

Pourquoi une telle lenteur ? Pour les détracteurs de la réforme, trois arguments principaux sont à prendre en compte : le manque de moyens éducatifs pour faire face aux naissances supplémentaires, la congestion des grandes métropoles et la limitation des ressources alimentaires.

Mais en réalité, pour Wang Feng, l’immobilisme actuel s’explique par un lien étroit entre la politique de l’enfant unique et le maintien au pouvoir des élites dirigeantes. Le planning familial, facilitant la redistribution des fruits de la croissance et réduisant la pression sur les ressources, l’emploi et l’éducation, a en effet contribué à la légitimation du pouvoir en place durant les trente dernières années. En outre, selon Shu Taifeng et Cao Jinwen, les dirigeants locaux, en particulier ceux des régions les plus peuplées qui craignent une explosion démographique, sont les principaux opposants à un assouplissement de la politique familiale. Certains observateurs craignent donc que le fait de confier aux autorités locales le choix du calendrier d’application ne conduise à amoindrir sa portée.

#### Impacts démographiques
De tels enjeux expliquent que l’évaluation des conséquences de la réforme sur la démographie chinoise fasse l’objet d’une importante bataille des chiffres. À commencer par le taux de fécondité actuel de la Chine, dont l’exactitude est très controversée. En octobre 2012, un rapport sur les « Tendances et évolutions de la population et l’ajustement de la politique démographique », publié par des démographes chinois, estimait le taux de fécondité chinois en 2010 à 1,5. Cependant, Yi Fuxian explique que le Bureau national de recensement, en surestimant la marge d’omission dans les classes d’âge les plus basses, a largement gonflé le chiffre du taux de fécondité<sup>[8](#myfootnote8)</sup>. Se basant sur les données des recensements de 2000 et 2010, Yi Fuxian affirme que le taux de fécondité en Chine serait en réalité de 1,18 en 2010, contre 1,22 en 2000.

Les conséquences de la politique d’assouplissement du contrôle des naissances s’avèrent encore plus difficiles à estimer. Selon un rapport de l’ONU cité dans l’article de Yi Fuxian, si la réforme proposée est mise en place, la population chinoise atteindra 1,4 milliard en 2020. Cependant, les articles étudiés ici estiment ces prévisions alarmistes et beaucoup trop élevées. Selon Shu Taifeng et Cao Jinwen, la réforme aura un impact limité sur la structure démographique chinoise. Le démographe Yuan Xin, professeur à l’université de Nankai et cité dans l’article de Caijing, affirme que la politique actuelle d’assouplissement du contrôle des naissances provoquera 50 millions de naissances supplémentaires d’ici à 2050, la population chinoise atteignant alors 1,385 milliard contre 1,340 aujourd’hui. Enfin, Yi Fuxian évalue à seulement 30 millions le nombre total de « naissances compensatoires » (补偿性出生, buchangxing chusheng) qu’une suppression totale du contrôle de naissances provoquerait.

Plusieurs arguments sont avancés par Yi Fuxian et Wang Feng pour relativiser les conséquences démographiques de la réforme. D’une part, selon Yi Fuxian, l’histoire mondiale a prouvé que les phénomènes de « baby boom » (婴儿潮, ying’er chao) ou de pic des « naissances compensatoires » sont toujours largement inférieurs au déficit de naissances qui les précède. L’auteur indique en outre que les expérimentations locales de la politique des « deux enfants » à Jiuquan (Gansu), Enshi (Hubei), Chengde (Hebei), et Yicheng (Shanxi)<sup>[9](#myfootnote9)</sup>, où le taux de fécondité était en moyenne de 1,5 enfant par femme en 2005, ont prouvé l’absence de phénomène de « rebond du taux de fécondité » (生育率反弹, shengyulü fandan).

D’autre part, pour Wang Feng, les évolutions sociétales ont très largement contribué à la diminution du taux de fécondité chinois. Selon lui, en Chine, de même que dans le reste du monde, la croissance économique est allée de pair avec une diminution du désir d’avoir de nombreux enfants. Ainsi, à Shanghai, où la majorité des parents sont eux-mêmes enfants uniques et donc autorisés à avoir deux enfants, le recensement de 2010 a montré que le taux de fécondité plafonnait à 0,7. De même, une étude menée par le Centre de recherche sur la transformation et la gouvernance sociale de l’université Renmin a montré que 60,5 % des individus choisiraient d’avoir un deuxième enfant, tandis que 27,2 % choisiraient de ne pas avoir un deuxième enfant, et que 12,2 % ne souhaitaient pas se prononcer.

Certes, le désir d’avoir peu d’enfants concerne essentiellement les grandes métropoles et beaucoup moins les zones rurales. Néanmoins, selon l’article de Caijing, les effets de l’ajustement actuel de la politique des naissances se feront principalement ressentir en ville. Dans les campagnes, en effet, très peu de parents sont enfants uniques, et les couples sont déjà autorisés à avoir un deuxième enfant lorsque le premier est de sexe féminin – sans compter les fréquentes naissances hors plan. En outre, l’exode rural massif affecte essentiellement les ruraux en âge de procréer. Les auteurs en concluent qu’une suppression pure et simple du contrôle des naissances aurait des conséquences très proches de celles suscitées par l’ajustement actuel.

#### Arguments économiques
Au-delà de la polémique sur les conséquences démographiques de la réforme, les principaux arguments portent sur les effets potentiellement positifs d’une augmentation du nombre des naissances pour le développement économique et social de la Chine.

Des arguments sociaux sont tout d’abord avancés par Wang Feng. Selon lui, la politique de l’enfant unique a eu des conséquences désastreuses sur les relations sociales et la structure familiale. Pour Yuan Xin, le passage d’une structure « 4-2-1 » à une structure « 4-2- 2 » permettra le retour à un meilleur équilibre intergénérationnel. Un autre problème social majeur, soulevé dans l’article de Caijing, est le déséquilibre du sexe-ratio. En effet, depuis 1994, celui-ci est systématiquement supérieur à 115. Or, selon Yuan Xin, la libéralisation du contrôle des naissances est la seule manière de diminuer ce fort déséquilibre, même si la réforme actuelle ne sera pas suffisante pour ramener ce taux à la normale.

Cependant, le principal argument de nos auteurs est économique. Shu Taifeng et Cao Jinwen expliquent tout d’abord que l’augmentation du nombre des naissances permettra d’accroître la population en âge de travailler. Celle-ci a culminé en 2011 à 940 millions et devrait décroître jusqu’en 2023, même en cas de réforme, en raison du décalage temporel entre les naissances supplémentaires et l’entrée sur le marché du travail. Selon Liang Jianzhang et Huang Wenzheng, une libéralisation totale des naissances permettrait de créer une force de travail supplémentaire, jeune et qualifiée, qui sera mieux à même que la génération précédente de répondre aux besoins croissants d’innovation et de valeur ajoutée dans l’économie chinoise.

Ensuite, les auteurs expliquent que les naissances supplémentaires permettront de relancer la demande, dans une économie qui souffre essentiellement de sous-consommation. Pour Yi Fuxian, il est nécessaire de mettre un terme à l’inquiétude des ménages quant à la capacité de leur enfant unique à assurer la prise en charge de leurs vieux jours, qui les incite à consommer le moins possible.

En conclusion, l’augmentation du nombre des naissances permettra (partiellement) d’atténuer les conséquences négatives du vieillissement de la population. Selon l’article de Caijing, en 2050, la population retraitée sera de 440 millions, soit un tiers de la population. L’augmentation de la population active soumise à l’impôt est donc essentielle pour réduire la pression financière causée par le vieillissement de la population. En conclusion, même si le texte de la réforme indique toujours un « maintien des principes de base de la politique nationale de la planification familiale » (坚持计划生育的基本 国策, jianchi jihua shengyu de jiben guoce), les auteurs espèrent donc qu’il ne s’agit que d’un premier pas vers une suppression totale des restrictions pesant sur les naissances en Chine.

<a name="myfootnote1">1</a>: Directeur du centre de recherche en politique publique du centre Tsinghua-Brookings. Il est également l’auteur de Un quart de l’humanité : mythologie malthusienne et réalités chinoises, 1700-2000, publié aux presses de Harvard en 1999.

<a name="myfootnote2">2</a>: Titulaire d’un PhD en économie de Stanford, cofondateur de Ctrip et actuellement professeur invité à l’école de management Guanghua de l’université de Pékin.

<a name="myfootnote3">3</a>: Titulaire d’un PhD en statistique de John Hopkins, PDG (et associé) d’une société financière à Pékin.

<a name="myfootnote4">4</a>: Démographe, professeur à l’université du Wisconsin et auteur de The Empty Nest of a Big Country (大国空巢, daguo kongchao), publié en 2007 à Hong Kong.

<a name="myfootnote5">5</a>: Journaliste à Caijing.

<a name="myfootnote6">6</a>: Journaliste spécialement dépêché par Caijing pour cet article.

<a name="myfootnote7">7</a>: « Commission nationale pour la santé et la planification familiale : politique des “deux enfants pour un parent enfant unique”, pas de calendrier national unifié », Xinhua, 16 novembre 2013.

<a name="myfootnote8">8</a>: En effet, une partie du calcul réalisé par le Bureau repose sur une estimation des « omissions » de déclaration de naissance. Or, selon l’auteur, les faits ont montré que ces estimations d’« omission », et donc les chiffres du Bureau, étaient systématiquement gonflés depuis 1994, ce que prouvent les écarts importants entre le nombre estimé de naissances en 1996 et 1997 (20,67 et 20,38 millions) et le nombre effectif de jeunes de 13 et 14 ans recensé en 2010 (15,89 et 15,23 millions).

<a name="myfootnote9">9</a>: L’existence de la zone d’expérimentation de Yicheng a été révélée dans un article du Nanfang zhoumo, en 2010. Cette expérimentation, mise en place dès 1985, a mené à un taux de fécondité en 2010 encore plus faible que la moyenne nationale.

*This article was first written for the China Analytica, which is the copyright holder.*
