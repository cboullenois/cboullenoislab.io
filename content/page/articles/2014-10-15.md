---
title: Le projet gouvernemental de cloud chinois
date: 2014-10-15
comments: false
paginate: true
showdate: true
tags: ["technology"]
categories: ["francais"]
description: "Le programme gouvernemental de transition des administrations vers le cloud computing représente une véritable révolution informatique pour les administrations centrales et locales."
featuredImage : "/cloud.jpg"

---
<img src="/cloud.jpg" width="30%"/>
<div id="légende">"Cloud de confiance" (可信云)</div>

> Le programme gouvernemental de transition des administrations vers le cloud computing représente une véritable révolution informatique pour les administrations centrales et locales

### Sources
+ Huang Yuting<sup>[1](#myfootnote1)</sup>, « Le cloud arrive », Xin shiji, 18 août 2014.
+ Tan Min<sup>[2](#myfootnote2)</sup>, « Une forte impulsion donnée par le programme d’acquisition de cloud administratif », Xin shiji, 18 août 2014.
+ Tan Min, « Les opérateurs courent après le cloud des entreprises d’État », Xin shiji, 18 août 2014.
+ Qu Yunxu<sup>[3](#myfootnote3)</sup>, « Le cloud redéfinit les règles du jeu », Xin shiji, 18 août 2014.
+ Zheng Peishan<sup>[4](#myfootnote4)</sup>, « Les multinationales donnent dans le cloud mixte », Xin shiji, 18 août 2014.

Le dernier Forum des services de cloud de confiance (可信云服务大会, kexin yun fuwu dahui) s’est tenu à Pékin les 15 et 16 juillet 2014, marquant la fin de l'élaboration du programme gouvernemental de transition des administrations vers le cloud computing<sup>[5](#myfootnote5)</sup>. Porté essentiellement par le ministère des Finances (财政部, caizheng bu) et le ministère de l’Industrie et des Technologies de l’information (工信部, gongxin bu), ce vaste programme représente une véritable révolution informatique pour les administrations centrales et locales, puisque celles-ci devraient sous peu migrer vers une utilisation beaucoup plus intensive des services de cloud pour leurs besoins informatiques. Pour accompagner cette transition technologique, un travail de définition des normes de certification des entreprises fournissant les services de cloud ainsi que des normes régissant les appels d’offre a été mené depuis un an et parvient désormais à sa conclusion. Ce programme gouvernemental devrait dynamiser un marché jusque-là encore peu exploité et créer une forte compétition entre les différents acteurs du cloud.

#### Une révolution informatique
Selon des sources proches du ministère des Finances citées par Tan Min, le programme gouvernemental de passage au cloud est presque achevé et les expérimentations officielles devraient bientôt commencer. Les organes centraux du gouvernement devraient être les premiers à acquérir les services de cloud computing, suivis par les gouvernements de province et par les municipalités.

Selon Ni Yi, directeur adjoint du Centre d'approvisionnement du gouvernement central (财政部的国采中心, caizheng bu de guocai zhongxin), le cloud ainsi que des programmes de gestion de métadonnées entreront dans le programme centralisé d’acquisitions du gouvernement et seront inscrits à son budget.

D’après le ministère des Finances, les objectifs principaux de ce programme sont la réduction des dépenses publiques et l’amélioration de l’efficacité des systèmes informatiques. Le gouvernement souhaite en outre soutenir le développement des services de cloud chinois. Un développement massif du secteur est ainsi attendu pour l’année 2015. Selon He Baohong<sup>[6](#myfootnote6)</sup>, le gouvernement dépensera désormais 10 % de son budget informatique annuel en services de cloud, soit 5 milliards de yuans. En outre, selon Huang Yuting, les prévisions les plus optimistes estiment que le programme du gouvernement pourrait faire croitre le marché du cloud chinois de 100 milliards de yuans<sup>[7](#myfootnote7)</sup>.

Plusieurs municipalités et ministères ont déjà commencé leur transition vers le cloud computing. Ville pionnière, Hangzhou a acheté des services de cloud en mars 2013, et 26 de ses 45 bureaux gouvernementaux fonctionnent désormais en cloud.

Selon Che Haixiang<sup>[8](#myfootnote8)</sup>, le système de cloud permet de « garantir le bon fonctionnement de tous les grands systèmes informatiques »<sup>[9](#myfootnote9)</sup>. Il garantit davantage de professionnalisme puisque sa gestion est déléguée à une entreprise spécialisée. Il améliore en outre la sécurité des données, qu’il permet de mieux partager et traiter. Enfin, il permet une réduction des coûts. Che Haixiang explique en effet que les dépenses en systèmes informatiques du gouvernement de Hangzhou étaient auparavant de 5 millions de yuans pour cinq ans, dont 3 millions étaient consacrés aux serveurs informatiques. À présent, les services de cloud ne coûtent que 300 000 yuans par an.

En ce qui concerne les organes centraux, le ministère de l’Éducation (教育部, jiaoyu bu), la commission de la santé publique (卫计委, weiji wei) ainsi que le bureau sismologique national (国家地震局, guojia dizhen ju) ont été les premiers à acquérir des services de cloud. Selon Tan Min, cette technologie se justifiait particulièrement dans le cas de l’administration sismologique, dont le site internet ne permettait pas de gérer l’« explosion » de la fréquentation en cas de catastrophe naturelle. Le cloud, beaucoup plus flexible, explique Tan Min, permet de s’adapter à la demande et ne s’expose pas à la saturation.

#### Système de certification
Selon Tan Min, l’essentiel du travail gouvernemental consiste désormais, d'une part, à mettre en place un système fiable de certification et, d'autre part, à établir des critères et des normes pour les procédures d’appel d’offre qui seront lancées par les différentes instances administratives. Ces deux priorités témoignent de la nécessité, pour le gouvernement chinois, de garantir la sécurité de ses données et la qualité des services proposés, face à une offre toujours plus importante et plus variée.

Le premier chantier a débuté en mai 2013, avec un forum organisé par le ministère de l’Industrie et des Technologies de l’information, auquel étaient invitées les principales entreprises de l’internet chinois, dont notamment China Mobile (中国移动, Zhongguo yidong), China Unicom (中国联通, Zhongguo liantong), China Telecom (中国电信, Zhongguo dianxin), Baidu (百度), Alibaba (阿里巴巴) et Tencent (腾讯, tengxun). Ces différents participants ont créé un groupe de travail pour le « cloud de confiance » (可信云服务工作组, kexin yun fuwu gongzuo zu) destiné à élaborer un processus de certification pour les entreprises de cloud.

Le second chantier a débuté en juin 2013, lorsque le centre d’acquisitions du ministère des Finances a, à son tour, organisé une conférence destinée à élaborer des normes pour les appels d’offre gouvernementaux concernant les services de cloud.

Depuis, neuf forums se sont tenus pour chacun des deux chantiers. Selon un participant à ces réunions, cité par Tan Min, les deux conférences se suivaient toujours de peu, souvent du jour au lendemain, et comprenaient de nombreux participants communs. Cependant, contrairement aux conférences sur les normes à l’achat, celles sur la certification comprenaient des entreprises étrangères et donnaient lieu à des débats très vifs.

Les premières décisions ont été rendues fin juin 2014 pour la certification et en juillet pour les normes d’acquisition. Pour pouvoir participer à des appels d’offre gouvernementaux, les entreprises de services de cloud devront passer la « certification du cloud de confiance » (可信 云认证, kexin yun renzheng), accordée par l’Alliance des centres de données de Chine (数据中心联盟, shuju zhongxin lianmeng)<sup>[10](#myfootnote10)</sup>.

Selon He Baohong, cette certification donne des garanties sur l’état général de l’entreprise et la qualité de son service de cloud, ainsi que son intégrité, l’authenticité de son engagement et son adhésion aux normes définies. La certification requiert en outre que les entreprises aient plus de six mois d’expérience dans les services de cloud.

Selon He Baohong, ce processus de certification attire de nombreuses entreprises, malgré son coût de 50 000 yuans. Deux vagues de certification ont déjà eu lieu, début 2014 puis en juin de la même année. 19 entreprises et 35 programmes ont pour l’instant obtenu la certification, parmi lesquelles China Telecom, China Mobile, Alibaba, Tencent, Baidu, Lanxun, Ucloud, 360, etc.

Cette certification n’est cependant que la première phase d’une rude compétition pour le marché du cloud chinois.

#### Compétition accrue
Le programme de cloud administratif a en effet accru la compétition qui oppose les différents acteurs de l’internet chinois pour remporter le marché du cloud. Parmi ces acteurs, trois grands types peuvent être distingués : les opérateurs (运营商, yunying shang) comme China Télécom, China Mobile et China Unicom, les grandes entreprises de services en ligne (互联网厂商, hulian wang changshang) comme Baidu, Tencent et Alibaba, et les entreprises multinationales (跨国厂商, kuaguo changshang) comme IBM, Microsoft, HP et Amazon.

Selon Qu Yunxu, les opérateurs ont un avantage naturel du fait qu’ils fournissent la connexion internet nécessaire au fonctionnement des services de cloud. Ils ont commencé depuis quelques années à construire des infrastructures (centres de données), mais leurs résultats en termes de cloud public<sup>[11](#myfootnote11)</sup> se sont révélés décevants. Selon le responsable d’une entreprise de développement de logiciels, le coût et la complexité de la transition vers le cloud sont bien plus importants chez les opérateurs internet que chez les entreprises de services en ligne comme Alibaba ou Tencent.

Les entreprises de services en ligne ont été en outre privilégiées jusqu’à présent par leurs bonnes relations avec les clients privés et les petites entreprises. Qu Yunxu explique que les liens tissés sont particulièrement forts avec les geeks chinois<sup>[12](#myfootnote12)</sup>, ce qui constitue un avantage important puisque ceux-ci représentent la majorité des créateurs de startup (“屌丝”创业者, « diaosi » chuangyezhe), en particulier dans le domaine informatique.

Néanmoins, le programme gouvernemental pourrait être l'occasion, pour les opérateurs, de revenir sur le devant de la scène. Wang Meng, directeur adjoint du centre d’opération des IDC (Internet Data Center) pour le département de la relation avec les entreprises d’État au sein de China Mobile, explique que les opérateurs internet étant des entreprises d’État, ils ont des relations privilégiées avec les administrations gouvernementales. Il en est de même pour le marché des entreprises publiques qui, après une longue période d’attentisme, devrait se développer très fortement dans les années à venir.

Ces liens privilégiés entre opérateurs et administrations publiques commencent à produire leurs effets. Qu Yunxu indique ainsi que, même au Zhejiang, le fief d’Alibaba, l’administration provinciale ainsi que celle de plusieurs villes ont passé un contrat avec China Telecom pour leurs services de cloud computing.

Dans cette compétition acharnée, les entreprises étrangères ont une place à part. Supérieures sur le plan technique et offrant un service plus stable et de meilleure qualité, elles ont pourtant de grandes difficultés à s’immiscer dans le marché chinois. D’une part, leur absence de relation avec les clients chinois et d’adaptation à la culture locale les handicapent sur le plan du cloud public. D’autre part, des exigences de sécurité nationale ont conduit le gouvernement à écarter les entreprises étrangères des appels d'offre pour le cloud administratif.

En revanche, Zheng Peishan explique que ces entreprises parviennent à développer des partenariats avec des entreprises chinoises. Microsoft, en partenariat avec Vianet (世纪互联, shiji hulian) et Capital Online (首都在线, shoudu zaixian), et Amazon, en partenariat avec China Broadband Capital Partners (中国宽带产业基金, zhongguo kuandai chanye jijin), ont entrepris de s’implanter sur le territoire chinois en 2013. Les multinationales commencent en outre à investir dans les centres de données en Chine : tandis qu’IBM a inauguré son premier centre de données à Hong-Kong en juin 2014, un centre appartenant à Amazon est actuellement en construction dans le Ningxia.

Une guerre des prix et de marketing s’engage donc entre les différents acteurs du cloud pour gagner du terrain dans cet immense marché qui s’ouvre actuellement en Chine, tant sur le plan public (gouvernements et entreprises d’État) que privé.

<a name="myfootnote1">1</a>: Éditeur à Xin shiji.

<a name="myfootnote2">2</a>: Journaliste à Xin shiji.

<a name="myfootnote3">3</a>: Journaliste à Xin shiji.

<a name="myfootnote4">4</a>: Journaliste à Xin shiji.

<a name="myfootnote5">5</a>: Il s’agit d’un ensemble de processus qui consistent à utiliser la puissance de calcul ou de stockage de serveurs informatiques distants à travers un réseau internet.

<a name="myfootnote6">6</a>: Directeur du centre sur internet au bureau des standards de l’institut de recherche du ministère de l’Industrie et des Technologies de l’information (工信 部研究院标准所互联网中心, gongxin bu yanjiu yuan biaozhun suo hulian wang zhongxin).

<a name="myfootnote7">7</a>: Cette prévision est très optimiste. Notons que les prévisions conservatrices estiment que le cloud administratif devrait créer un marché d’au moins 5 milliards de yuans.

<a name="myfootnote8">8</a>: Directeur de l’office de la promotion de l’information à la commission municipale de l’économie et de l’information de Hangzhou (杭州市经信委信息化推进 处, hangzhou shi jingxin wei xinxihua tuijin chu).

<a name="myfootnote9">9</a>: 能够保证各大系统的正常运行, nenggou baozheng ge da xitong de zhengchang yunxing.

<a name="myfootnote10">10</a>: Officiellement créée le 16 janvier 2014 sous l’autorité du ministère de l’Industrie et des Technologies de l’information, l’Alliance comprend 71 entreprises dont la majorité sont chinoises.

<a name="myfootnote11">11</a>: Au contraire du cloud privé, utilisé par une seule entreprise, et du cloud administratif, utilisé par les organes gouvernementaux, le cloud public est partagé entre de nombreux utilisateurs.

<a name="myfootnote12">12</a>: “屌丝”, « diaosi ». Cette terminologie, d’abord utilisée pour parler des classes les plus basses de la population qui cherchent dans les jeux vidéos un échappatoire à leur vie quotidienne, est désormais repris par une grande partie de la jeunesse chinoise, éduquée technologiquement et utilisant abondamment internet.

*This article was first written for the China Analytica, which is the copyright holder.*
