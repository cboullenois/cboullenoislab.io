---
title: Vers une tutelle autonome de la SASAC
date: 2014-08-15
comments: false
paginate: true
showdate: true
tags: ["politics"]
categories: ["francais"]
description: "La réforme du système des entreprises d’État est devenue une priorité en Chine après le IIIe Plénum du XVIIIe Congrès du PCC, en octobre 2013. "
featuredImage : "/xiaoyaqing.jpg"

---
<img src="/xiaoyaqing.jpg" width="50%"/>
<div id="légende">Xiao Yaqing (肖亚庆), directeur de la SASAC depuis 2016. Quand cet article a été écrit, le directeur était Zhang Yi.</div>

### Sources
+ Wang Xin<sup>[1](#myfootnote1)</sup>, « Reconstruction du système de gestion des actifs d’État », Xin shiji - New Century, 16 juin 2014.
+ Zhang Qihua<sup>[2](#myfootnote2)</sup>, « Réforme des entreprises publiques : extension massive des versions chinoises de Temasek ? », Qiye guancha bao
+ The Enterprise Observer, 29 mai 2014. – Li Bin<sup>[3](#myfootnote3)</sup>, « Comment éviter l’opposition entre entreprises d’État et économie de marché ? », Zhengquan shibao - Securities Times, 23 septembre 2013.
+ Xu Baoli<sup>[4](#myfootnote4)</sup>, « La formation des sociétés d’investissement et d’exploitation des capitaux publics », Caijing, 11 décembre 2013.

> La réforme du système des entreprises d’État est devenue une priorité en Chine après le IIIe Plénum du XVIIIe Congrès du PCC, en octobre 2013.

Outre l’introduction de capitaux privés dans les entreprises d’État, la réforme porte notamment sur le rôle de la Sasac (国务院国 有资产监督管理委员会, guowuyuan guoyou zichan jiandu guanli weiyuanhui, généralement appelé 国资委, guoziwei), l’organisme de régulation et de contrôle des capitaux publics. Les articles étudiés ici se font tous l’écho de la nécessité de réorganiser en profondeur le système de supervision des entreprises publiques. Pour les auteurs, il s’agit avant tout de réaffirmer conjointement le choix de l’économie de marché et le rôle des entreprises d’État dans l’économie chinoise.

#### Un secteur à nouveau en perte
La réforme, selon les journalistes et les spécialistes interrogés, répond à un besoin urgent d’améliorer la compétitivité des entreprises d’État. En effet, pour Li Bin, les mauvaises performances des entreprises d’État sont essentiellement liées à l’ingérence du gouvernement dans ces dernières. Les organes du gouvernement central ainsi que les gouvernements locaux interviennent dans la gestion des entreprises, leur procurent un soutien arbitraire, et réclament de celles-ci qu’elles se mettent au service de buts politiques. Le journaliste qualifie cette situation de « conflit entre ceux qui ont à la fois les rôles d’“arbitres” et de “joueurs” » (“裁判员”与“球 员”之间的角色冲突, caipanyuan yu qiuyuan zhijian de juese chongtu).

Selon Zuo Fangsheng, vice-président du Conseil de recherche sur la réforme et le développement des entreprises, cité par Zhang Qihua, ce problème découle directement des fonctions de la Sasac, qui cumule les rôles d’investisseur des capitaux publics, de législateur et de superviseur. Des collusions d’intérêts interviennent nécessairement entre ces trois fonctions, qui devraient donc être séparées.

Xu Baoli souligne en outre que la Sasac outrepasse en pratique les pouvoirs qui lui sont officiellement conférés. Outre ses trois fonctions officielles, elle se charge de nommer les cadres, de réguler les salaires, d’évaluer les résultats, de décider de la stratégie des entreprises, et de leurs répartir les bénéfices. Son rôle est quasiment celui d’un conseil d’administration qui gèrerait directement toutes les entreprises d’État.

Pour Lin Bin, cette situation aboutit à des inégalités en matière de salaires, ainsi qu’à un fort laxisme dans l’application du droit des entreprises. Elle provoque également des monopoles excessifs et nuit au bon fonctionnement de l’économie de marché. Pour l’auteur, l’interventionnisme du gouvernement revient donc à un recul en matière économique – illustré par l’explosion du taux d’endettement des entreprises publiques –, mais aussi sociale – illustré par des injustices croissantes et des problèmes de gouvernance.

#### Le modèle singapourien
La réforme proposée lors du IIIe Plénum du XVIIIe Congrès vise donc à diminuer l’intervention du gouvernement dans la gestion des entreprises d’État. Selon Xu Baoli, elle consistera à « passer d’un système dual à un système triple » (从两层架构向三 层架构转变, cong liang ceng jiagou xiang san ceng jiagou zhuanbian), dans lequel des sociétés d’investissement ou d’exploitation des capitaux publics, proches du modèle singapourien, joueront le rôle d’intermédiaire entre la Sasac et les entreprises d’État.

Ces sociétés d’investissement seront soit créées de toutes pièces, soit issues de la réorganisation d’entreprises déjà existantes. Elles fonctionneront comme des holdings et auront pour mission de gérer et de répartir les capitaux d’État. Elles seront soumises à la supervision de la Sasac et auront un rôle d’investisseur et d’administrateur vis-à-vis des entreprises d’État. Leurs droits en tant que porteur de capital seront les mêmes que ceux des investisseurs privés, et seront pareillement soumis aux lois sur les entreprises.

Wang Xin explique que le capital de chaque entreprise d’État sera possédé par environ trois de ces sociétés d’investissement, afin d’éviter une trop grande concentration des pouvoirs. De leur côté, les sociétés d’investissement participeront au capital d’environ une dizaine d’entreprises d’État, à hauteur d’au moins 20 %, pour éviter l’éparpillement de l’actionnariat et du pouvoir de décision. Par ailleurs, elles seront chacune l’actionnaire majoritaire dans trois ou quatre d’entre elles.

Selon Zhang Qihua, ces holdings serviront donc de « mur protecteur » (防火墙, fang huo qiang) entre les organes du Parti et la Sasac d’une part, et les entreprises d’État d’autre part. Elles permettront en effet de réduire l’influence de l’administration et du monde politique sur les entreprises, et d’accroître leur autonomie dans le cadre de l’économie de marché.

Enfin, Xu Baoli explique que cette clarification du rôle des porteurs de capitaux permettra de mener à bien la réforme de la propriété mixte<sup>[5](#myfootnote5)</sup>, qui verra des capitaux privés investis dans les entreprises d’État.

Il ajoute que, dans ce nouveau système, la Sasac verra son rôle profondément modifié. Seules les fonctions de réglementation et de supervision lui resteront. Elle sera amenée à contrôler uniquement les sociétés d’investissement et d’exploitation des capitaux d’État, et non plus les entreprises d’État elles-mêmes. Selon Zhang Qihua, la Sasac a en outre vocation à se professionnaliser et à modifier sa structure interne, en se focalisant sur sa fonction de supervision.

Zhang Qihua indique également que cette réforme s’inspire clairement du système singapourien et notamment de Temasek Holdings (淡 马锡, danmaxi), la société d’investissement de l’État singapourien. Temasek, qui a des actions dans la plupart des entreprises publiques singapouriennes, ainsi qu’à l’étranger, joue un rôle identique à celui d’un actionnaire privé. Selon l’auteur, son conseil d’administration, relativement indépendant, contrebalance le pouvoir du gouvernement dans la gestion des entreprises d’État. Le PDG de Temasek a en effet le pouvoir d’ignorer les décisions du gouvernement. En outre, les dix membres du conseil d’administration de Temasek viennent tous d’entreprises privées.

#### Un horizon rapproché
La question du calendrier de mise en place des réformes est soulevée par Zhang Qihua. Selon lui, des proches de la Sasac et du Conseil d’État affirment que trois à cinq sociétés d’investissement ou d’exploitation des capitaux d’État devraient être crées dans les trois prochaines années à titre d’expérimentation, tandis que, à l’horizon 2020, environ trente sociétés de ce type devraient voir le jour. Cette réorganisation a été mise à l’ordre du jour des missions du « Small leading group de la Sasac pour l’approfondissement global de la réforme »<sup>[6](#myfootnote6)</sup> (国资委全面深化改 革领导小组, guoziwei quanmian shenhua gaige lingdao xiaozu) pour 2014. Sept milliards de yuans (830 millions d’euros) ont été prévus dans le cadre du « budget d’exploitation pour les capitaux d’État » (中央国有资本经营支出预算, zhongyang guoyou ziben jingying zhichu yusuan) pour établir ces sociétés d’investissement. Enfin, la Sasac est en train d’élaborer un programme précis dont le contenu comprendra les modalités de réorganisation ou de création des sociétés d’exploitation et d’investissement des capitaux d’État.

> "La réforme proposée lors du IIIe plénum du XVIIIe Congrès vise à diminuer l'intervention du gouvernement dans la gestion des entreprises d'Etat."

D’après le journaliste, la première étape consistera à choisir quelques sociétés existantes et à les réorganiser pour en faire des sociétés d’investissement et d’exploitation des entreprises publiques. Le choix sera effectué lors du second semestre 2014 en fonction de plusieurs critères : il devra s’agir de groupes valant de 10 à 50 milliards de yuans, dont la plupart des activités sont cotées en bourse, aux « droits de propriété sur les actifs clairement identifiés » (股权产权清晰, guquan chanquan qingxi), au « fort taux d’actifs circulants » (流动资产比例大, liudong zichan bili da), et couvrant une grande variété de secteurs économiques. Ces conditions concernent une trentaine d’entreprises. Néanmoins, selon le responsable de la recherche au China Enterprise Research Institute, Li Jin, interrogé par Zhang Qihua, certaines de ces entreprises semblent particulièrement susceptibles de devenir les premières sociétés d’investissement et d’exploitation, et de servir de pilotes dans la mise en place de la réforme. Il cite notamment China Resources (华润, huarun), Baosteel (宝钢集团, baogang jituan), Chengtong (诚通), et Petrochina (中石油, zhongshiyou).

Par ailleurs, quelques grandes entreprises d’État n’ont pas attendu la mise en place de la réforme pour s’orienter vers une nouvelle pratique de gestion des capitaux. Li Jin explique ainsi que China National Building Materials (中国建材, Zhongguo jiancai) a déjà engagé plus de 70 personnes pour établir une entreprise d’investissement qui ne s’occupe que de l’investissement des capitaux et de l’élaboration des projets (项目建设, xiangmu jianshe), sans se charger de la gestion des affaires courantes. China Merchants Group (招商局, zhaoshangju) a également fondé en 2012 une entreprise de gestion de capitaux qui gère environ 20 milliards de yuans et couvre des secteurs aussi variés que les infrastructures, l’immobilier, l’agriculture ou encore la santé. Enfin, certaines entreprises publiques locales, dont la taille plus petite permet une réorganisation plus rapide, ont déjà commencé à chercher des « plateformes pour l’investissement et l’exploitation de leurs capitaux [publics] » (国有资本投资运营平台, guoyou ziben touzi yunying pingtai). Selon Li Jin, ces embryons de sociétés d’investissement et d’exploitation des capitaux publics devront cependant améliorer leurs compétences et leurs procédures.

En juillet, un groupe de six entreprises d’État, parmi lesquelles la SDIC (国投, guotou), COFCO (中糧, guoliang), Sinopharm (国药控 股, guoyao konggu), China National Building Materials (中国建材, zhongguo jiancai), la CECEP (中国节能, zhongguo jieneng) et le groupe Jihua (际华集团, jihua jituan) a en outre été choisi pour expérimenter une gestion plus indépendante du politique ainsi que la réforme de la propriété mixte des capitaux<sup>[7](#myfootnote7)</sup>.

Malgré ces avancées, Xu Baoli souligne que la portée des réformes risque de rester limitée. Selon lui, les décisions importantes concernant l’administration des plus grandes entreprises d’État dépasseront les compétences des sociétés d’investissement. La Sasac ne renoncera pas avant plusieurs années à son rôle essentiel de gestionnaire des capitaux publics pour un certain nombre de grandes entreprises d’État, tandis qu’un système dual et un système triple coexisteront pendant quelque temps.

<a name="myfootnote1">1</a>: L’auteur travaille à Bank of China International (中银国际, zhongyin guoji), une filiale de la Bank of China qui effectue des investissements financiers et propose des services de courtage.

<a name="myfootnote2">2</a>: Journaliste à Qiye guancha bao.

<a name="myfootnote3">3</a>: Journaliste à Zhengquan shibao.

<a name="myfootnote4">4</a>: Directeur du centre de recherche sur la compétitivité de la Sasac.

<a name="myfootnote5">5</a>: Il a été décidé lors du IIIe Plénum du XVIIIe Congrès du PCC que les capitaux non étatiques seront autorisés à prendre part à des projets comprenant des investissement effectués par des capitaux publics.

<a name="myfootnote6">6</a>: Il s’agit d’un comité interne à la Sasac, dirigé par le directeur de la Sasac Zhang Yi.

<a name="myfootnote7">7</a>: D’après l’article de Chen Huiying, « La SASAC annonce “quatre projets de réforme” : l’attention se porte sur les fonctions des conseils d’administrations », Caijing, 15 juillet 2014.

*This article was first written for the China Analytica, which is the copyright holder.*
