---
title: The mobile revolution and China’s internet giants
date: 2014-06-15
comments: false
paginate: true
showdate: true
tags: ["technology"]
categories: ["briefs"]
description: "For the past two years, the BAT (Baidu, Alibaba and Tencent) have had to adjust to face the same mobile revolution that is shaking up the IT industry worldwide."
featuredImage : "/bat2.jpg"

---
<img src="/bat2.jpg" width="50%"/>


> For the past two years, the BAT (Baidu, Alibaba and Tencent) have had to adjust to face the same mobile revolution that is shaking up the IT industry worldwide.

### Sources
+ Chen Lin, “Big changes in the world of China’s internet”, Caijing, 19 April 2014<sup>[1](#myfootnote1)</sup>.
+ Song Wei and Chen Qingchun, “The BAT are worried”, Caijing, 19 April 2014<sup>[2](#myfootnote2)</sup>.
+ Song Wei, “The fight for the autonomy of start-ups”, Caijing, 19 April 2014.

The internet industry in China is dominated by three big names: Baidu (百度), Alibaba (阿里巴巴), and Tencent (腾讯). Baidu specialises in “search” (搜索, sousuo), Alibaba in “e-commerce” (电商, dianshang), and Tencent in “social networks” (社交, shejiao). Together, these companies are referred to as the BAT. For the past two years, the BAT have had to adjust to face the same mobile revolution that is shaking up the IT industry worldwide. Their positions within China’s internet sector have been turned upside down by the need to come to terms with the rapid shifts in the industry. Some observers predict that new, mobile-era companies will emerge to overtake the old guard. Others say that the BAT have become increasingly aggressive in their efforts to adapt to the new competition.

#### Changing ecosystem
For two years now, the BAT have been seriously concerned about the risk of losing their internet monopoly because of the the mobile web revolution. “Mobile traffic” (移动流量, yidong liuliang) is inherently more scattered and harder to control than desktop traffic. Song Wei and Chen Qingchun say that the success of the BAT will be determined by their ability to build “open platforms” (开放平台, kaifang pingtai) that can integrate their various products.

Since 2011, the fear of being overtaken by more innovative companies has pushed the BAT to adopt extremely aggressive strategies to preserve their control of the market. Over the past three years, a greater number of mergers and acquisitions have taken place than in the entire previous decade, and the economic importance of the mergers has also been greater than in previous years. Within these three years, Alibaba has acquired or become a shareholder in 30 companies, Tencent in 40 companies, and Baidu in more than 30 companies. Each of the internet giants has been in fierce competition with the others for all these acquisitions, which has resulted in over-inflated purchase prices.

Each of the three major companies has its own strengths and weaknesses within this constantly changing “internet ecosystem” (互联网生态, hulian wang shengtai).

#### Baidu
Baidu was founded in 2000 and is now the largest search engine in China. It has developed its own applications in search, online mapping, and mobile assistance, as well acquiring e-commerce (Nuomi, 糯米), travel (Qunar, 去哪儿), and video (iQiyi, 爱奇艺) applications. Baidu currently owns 14 applications that together reach over 100 million users. In April 2014 the company launched the Baidu Wallet (百度钱包, baidu qianbao), giving it a presence in the mobile payments market. Baidu also plays an important role in the O2O sector (online-to-offline, which involves online maps in particular). This capability is a major asset in its competition with the other two giants. In 2013 Baidu still had a 72 percent market share in mobile search. However, it is now struggling not to lose its footing, following the development of a mobile search market that is “intrinsically fragmented” (移动互联网流量天生是分散的, liudong hulian wang tiansheng shi fensan de). Because of this, even though Baidu was the most important internet company when desktop computers were the main means of internet access, its share value has experienced a relative decline in recent years. The company is now worth just half of competitor Alibaba’s estimated value<sup>[3](#myfootnote3)</sup>.

Baidu’s answer to the challenge has been to develop LBS (location-based services). These services integrate search, online mapping, and social networking, and aim to redefine “search” for the mobile environment. Baidu has put significant investment into the project, which has helped it to attract large amounts of traffic to its LBS, and in particular, to its mapping and e-commerce applications, Baidu Maps and Nuomi.

For this reason, Song and Chen say, many observers now see Baidu as the most innovative of the BAT and, as a result, the best placed to stay on top of future trends. However, Song and Chen say that Baidu remains “conservative” (保守派, baoshou pai) in its acquisitions. The company spent a lot of money to acquire a controlling stake in the large mobile app store, 91 Wireless, but it has generally not tried to compete with the others in the race to acquire start-ups. This means that it is now at risk of being marginalised by the other two internet giants. Even so, Song and Chen say that the company’s large cash reserves should allow it to catch up by acquiring several companies in 2014.

#### Alibaba
Alibaba is the biggest player in Chinese e-commerce. It is commercially aggressive and it has grown rapidly – 2.27 times faster than competitor Tencent. In 2013 its revenues exceeded those of both Baidu and Tencent.

With an estimated value of between $150 billion and $200 billion, the listing will be one of the most valuable in history Founded by Jack Ma in 1998, Alibaba has benefited from the unique offering of its main e-commerce site, Taobao. Unlike US company Amazon, which sells directly to consumers, Taobao leases space on its website to vendors and advertisers. It does not take responsibility for product quality, shipping, or after-sales support. Through its Taobao and Tmall sites, Alibaba currently controls 80 percent of China’s online retail market, equivalent to 5 percent of the country’s total retail sales. And its Alipay site gives it a key position in the online payment market.

The mobile revolution has presented Alibaba with some serious challenges. The company has struggled to create an open portal capable of integrating its different products. In spite of significant investment, the company’s Aliyun (阿里云) mobile operating system did not take off. And Song and Chen argue that the constant upheaval in Alibaba’s offline sector, including changes in CEO in March 2013 and in March 2014, has prevented the establishment of clear guidelines for growth.

Song and Chen interview Long Wei, the co-founder of ratings and review site Dazhong Dianping (大众点评), who says that Alibaba must also find a way to innovate within its “’light commercial’ model” (“轻商业”的模式, qing shangye de moshi). The important O2O sector requires much larger and better-supplied sales teams than Alibaba currently uses. However, Song and Chen interview a former Alibaba vice-president who says that the group is reluctant to make changes to a model that has so far enabled it to make huge profits.

Trying to hedge against these uncertainties, Alibaba has acquired and invested in up-and-coming companies such as group deal site Meituan (美团), location-based instant messaging service Momo (陌陌), mobile browser UCWeb, microblogging site Xinlang Weibo (新浪微博), and online mapping app AMAP (高德).

Alibaba wants to create infrastructure that combines both online and offline elements. To this end, in March 2014 Alibaba announced its acquisition of a 9.9 percent stake in the bricks-and-mortar Yintai (银泰) retail chain. However, this type of partnership may cause problems, because the interests of the internet majors and of offline groups may differ. Song and Chen note that it may be difficult to safeguard the interests of Yintai’s shops and website at the same time as protecting Alibaba’s global interests.

#### Tencent
Tencent was founded in 1998. The company owns the two largest social networks in China, QQ and Weixin, and it is also the world’s largest video game company. QQ has around 800 million users in China. The estimated value of Weixin is around $64 billion, three times the price that Facebook paid to acquire messaging application WhatsApp in February 2014. But most of Tencent’s income comes from its innovative freemium online gaming business model: Tencent offers free-to-play games in which players make microtransactions to improve their gaming experience.

The freemium model is the basis of Tencent’s current success, but slower growth in the mobile gaming market may soon reduce profits. Song and Chen interview the CEO of social shopping site Mogujie (蘑菇街), Chen Qi, who says that Tencent will have a hard time monetising its social networks, even though they are formidable communication tools. Going forward, monetisation is one of the key components of the company’s strategy. Song and Chen quote a high-ranking group employee as saying that QQ’s online gaming and entertainment services make it the company’s most promising application for monetisation. More caution will be needed in adding online payments to Weixin, if its popularity is not to suffer.

> Since 2011, the fear of being overtaken by more innovative
companies has pushed the BAT to adopt extremely aggressive strategies to preserve their control of the market

Tencent’s acquisitions strategy has allowed it to compete with Baidu and Alibaba on their own ground. In September 2013, the company spent $448 million acquiring a stake in Sogou (搜狗), China’s third-largest search engine. In March 2014 it agreed a partnership with Jingdong (京东), one of the largest e-commerce companies in China, which should help Tencent to compete with Alibaba’s Taobao.

#### Potential challengers
Some second-tier companies that were created as a result of the mobile revolution are attempting to challenge the dominance of the BAT. These innovative companies are experiencing exceptional growth.

Smartphone manufacturer Xiaomi (小米) is one of the biggest potential threats to the BAT companies. Xiaomi has only recently launched e-commerce operations and begun to construct a mobile internet platform. But its dominance in the manufacture of internet terminals has given it a significant advantage in the technological battle that will help to decide the fate of the BAT.

Qihoo 360, which provides antivirus software, technical support, and internet routers, has developed a unique entry point for mobile internet. Its rapid growth and its new search engine also give it the tools to provide real competition to the BAT.

Finally, some companies in the new market of “local services” (本地服务, bendi fuwu) are also growing fast and could produce a future internet giant. These companies include Dazhong Dianping and Meituan. Both companies have agreed to partnership terms with the BAT, but Long Wei tells Song and Chen that the two companies have retained enough independence to allow them to continue autonomous growth.

#### Start-ups
Below these emerging powerhouses, many smaller start-ups have original product offerings and strengths. However, the BAT’s aggressive policies have often worked to prevent their development.

The “taxi wars” (打车大战, dache dazhan) are a good example of how the BAT has discouraged the growth of smaller companies. In 2012, around 30 companies created applications to connect customers with taxis in major Chinese cities. Alibaba invested in one, Didi Dache (嘀嘀打车), and Tencent in another, Kuaidi Dache (快的打车). The others obtained investments from independent venture capital funds. Didi Dache and Kuaidi Dache benefited from substantial subsidies and huge traffic from their BAT patrons, and after a short price war, the companies that were not backed by the BAT majors collapsed.

Song and Chen say that the two companies supported by the internet giants did not fare much better than their competitors. Kuaidi Dache and Didi Dache quickly became empty shells, dependent on the parent company and with no users of their own. One start-up founder interviewed by Song says that any company facing a buy-out proposal should ask whether the company is likely to become an essential part of the buy-out group, or whether it will simply be subsumed by it.

Even in this difficult environment, some companies have succeeded in growing in the shadow of the internet giants. Mogujie, an e-commerce site that redirects users to Taobao and Tmall, is one such example. Alibaba’s buy-out attempts have all failed, and the partnership agreed between Mogujie and Tencent’s Weixin in August 2013 allowed the start-up to retain some autonomy.

However, this kind of independent growth is becoming increasingly rare. An investor interviewed by Song Wei says that the internet giants are replacing market forces in determining the success or failure of start-ups. In this way, the BAT are seriously holding back innovation. The next few years will be both difficult and pivotal for the BAT. The three giants will have to deal with competition within their own ranks as well as from up-and-coming internet companies. At the same time, they will have to adapt to a mobile market that is not a natural environment for the internet giants.

<a name="myfootnote1">1</a>: Chen Lin is a journalist for Caijing.

<a name="myfootnote2">2</a>: Song Wei and Chen Qingchun are staff journalists for Caijing.

<a name="myfootnote3">3</a>: In March 2014 Alibaba announced that it would list on the US stock exchange.

*This article was first written for the China Analytica, which is the copyright holder.*
