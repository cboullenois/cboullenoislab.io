---
title: La gestion du séisme de Ya’an par le gouvernement chinois
date: 2013-06-15
comments: false
paginate: true
showdate: true
tags: ["governance"]
categories: ["francais"]
description: "Le 20 avril 2013, le Sichuan a été secoué par un séisme de magnitude 6,6, qui a essentiellement touché le district de Lushan de la ville de Ya'an, faisant 196 morts, 21 portés disparus et 11 470 blessés."
featuredImage : "/yanan.jpg"

---
<img src="/yanan.jpg" width="50%"/>

### Sources

+ Hu Ge, Wang Xiaoqing, Wang Jing et Zhang Tao, « Le grand défi du tremblement de terre de Ya’an », Century Weekly, 29 avril 2013.
+ Lan Fang et Cai Xie, « Wang Zhenyao : les ONG sont-elles trop nombreuses pour être efficaces ? », Caixin, 27 avril 2013.
+ Zhang Zhouyi, Xu Qianchuan et Ling Xin, « Le grand test de la gestion du séisme de Lushan », Caixin, 5 mai 2013.
+ Chen Xiaoshu, « Grande expérimentation du système d’alerte précoce des tremblements de terre : le système est encore au stade de projet », Caijing, 20 mai 2013.
+ Hua Lu, « Publique, collective, individuelle : trois échelles de l’aide », Caijing, 5 mai 2013.


Le 20 avril 2013, le Sichuan a été secoué par un séisme de magnitude 6,6, qui a essentiellement touché le district de Lushan de la ville de Ya'an, faisant 196 morts, 21 portés disparus et 11 470 blessés. Pour les journalistes chinois, impossible de ne pas établir une comparaison avec le séisme de 2008, à Wenchuan, qui avait été particulièrement destructeur.

Si, pour tous les observateurs, un réel progrès a été réalisé dans la gestion de la crise par le gouvernement chinois, ce séisme a néanmoins suscité des questions sur les possibilités d'améliorer encore l'efficacité du système et sur l'état de préparation de la région à l'éventualité d'un nouveau séisme. La catastrophe a également mis en avant le rôle nouveau des acteurs de la société civile dans l'organisation des secours.

#### Système de gestion d'urgence
Wang Zhenyao, ancien directeur du département de gestion d'urgence des catastrophes au ministère des affaires civiles, et directeur de l'Institut de recherche sur la philanthropie à l'école normale supérieure de Pékin, affirme que « cette fois, le système de gestion d'urgence a été mis en place très rapidement ».

En effet, les articles étudiés font tous état d'une nette amélioration dans l'efficacité de la gestion gouvernementale des secours. Cette amélioration tient à l’expérience du séisme de Wenchuan et à l'enclenchement d'une « procédure d'urgence » (应急救灾系统, yingji jiuzai xitong) éprouvée lors d'un exercice général en 2012.  

Dans les instants qui ont suivi le séisme, le système de secours d'urgence a été lancé au sein du gouvernement provincial du Sichuan à Chengdu, considéré comme l'institution la plus expérimentée de Chine en matière de gestion des séismes. Deux heures après, le Conseil d’État tenait une réunion avec les responsables des différents départements impliqués dans la gestion du séisme, tandis que la première troupe militaire de la région de Chengdu parvenait sur la zone sinistrée.

D'un point de vue organisationnel, le Bureau de gestion d'urgence provincial du Sichuan (四川省应急办指挥中心, sichuansheng yingji ban zhihui zhongxin) est responsable devant le bureau de gestion d'urgence du Conseil d’État (国务院应急办, guowuyuan yingji ban) et oriente les secours civils et militaires au niveau provincial et municipal. Il forme une structure en réseau et collabore avec des membres du ministère des finances, des affaires civiles, des transports, de la sécurité et de la commission nationale de la réforme et du développement.

Selon les articles, l'efficacité de la gestion des secours tient aussi à la rapidité avec laquelle le gouvernement a pris conscience de l'ampleur et de la gravité du séisme. Le « niveau un » de gestion d'urgence, s'appliquant aux « catastrophes majeures » (特别重大的灾害, tebie zhongda de zaihai), a été décrété trois heures seulement après le début du séisme, alors que cinq heures avaient été nécessaires en 2008. Ce progrès démontre l'efficacité de plus en plus grande du système de remontée et d'analyse des informations.

#### Mauvaise gestion
Bien que reconnaissant le net progrès de la gestion des secours, les articles en soulignent également ses faiblesses, et notamment l'encombrement des routes dû à la mauvaise gestion des transports.

En effet, malgré des mesures prises pour améliorer l'accès par voie rapide entre Chengdu et Ya'an, certains villages des alentours de Ya'an sont restés longtemps isolés : les véhicules apportant les équipements de secours et les biens de première nécessité ne pouvaient pas y accéder, en raison notamment de glissements de terrain et de coulées de pierres qui avaient endommagé les routes. A cela s'est ajouté le manque d'hélicoptères disponibles (importés pour la plupart des Etats-Unis ou de Russie), constituant pourtant un élément essentiel des premiers secours dans cette région montagneuse.

Ces difficultés d'accès sont en partie dues à une mauvaise gestion des priorités de transport. Les permis spéciaux permettant l'accès à la zone sinistrée ont en effet été distribués de manière laxiste, selon les reporters de Caijing, ce qui a provoqué l'arrivée en masse de volontaires. Les routes, trop étroites, se sont trouvées rapidement engorgées, et les premiers secours ont eu beaucoup de difficulté à parvenir sur les lieux de la catastrophe. Wang Zhenyao affirme par conséquent que la clarification des priorités de transport permettrait d'améliorer la gestion des catastrophes en Chine.

Selon l'article de Caijing, le séisme de Ya’an a mis en évidence les limites du système en réseau mis en place au niveau du gouvernement provincial pour répondre à une catastrophe. Si celui-ci s'avère en effet très efficace pour orienter et organiser les secours depuis le haut de la pyramide hiérarchique, elle ne permet pas de prendre en compte l'ensemble de la zone sinistrée, laissant de nombreux villages isolés.

#### Prévention en amont
Les difficultés d'accès des secours dans les zones sinistrées ont mis en évidence le manque de prévention et le mauvais respect des normes de construction antisismiques en amont.

Dans le bourg de Longmen, qui compte 25 tués et 28 blessés graves, les villageois affirment au micro de Caixin ne jamais avoir participé à des exercices d'évacuation. Pourtant, un cadre du district indique que ceux-ci étaient effectués une fois par an et qu'une documentation concernant les procédures à suivre en cas de séisme avait été envoyée. Cette contradiction s'explique en partie par le fait que dans ce village, comme dans beaucoup d'autres, la plupart des travailleurs actifs partent travailler en ville. Seuls restent les enfants et les personnes âgées, qui ne savent pour la plupart pas lire les instructions envoyées par le gouvernement local.

Hua Lu compare cette situation avec celle du Japon, où la lutte contre les risques naturels passe avant tout par la prévention et par l'accroissement de la capacité des niveaux locaux à autogérer des situations de crise sans attendre un hypothétique secours du gouvernement central. Il exprime ainsi clairement son souhait de voir se mettre en place une politique similaire en Chine.

En ce qui concerne les normes de constructions antisismiques, les articles font état d'une amélioration du corpus légal, mais soulignent que les textes sont appliqués de manière très insuffisante, notamment dans les campagnes. Les « normes de conception parasismique des bâtiments » (建筑抗震设计规范, jianzhu kangzhen sheji guifan) ont en effet été révisées en 2010 par le ministère des constructions, en réaction aux dégâts causés par le séisme de Wenchuan. Selon cet amendement, les bâtiments du district de Lushan devaient être équipés pour résister à un séisme de force 7 et ceux des villages voisins, Tianquan et Baoxing, pour un séisme de force 8.
Au-delà de la reconstruction de grands bâtiments (écoles ou hôpitaux), les gouvernements locaux ont beaucoup investi, après le séisme de 2008, dans l’amélioration des bâtiments et des infrastructures de transport, et dans le renforcement des digues et des ponts.

Malgré ces mesures, les journalistes indiquent qu’au jour du séisme, le district de Ya’an faisait encore preuve de niveaux de sécurité plus bas que ceux de Wenchuan en 2008. Les normes antisismiques ne concernaient que les bâtiments publics, tandis que les autres constructions étaient beaucoup moins réglementées, notamment dans les campagnes. En outre, les fonds destinés à la reconstruction ont été en grande partie dirigés vers les villes, délaissant les villages. L'article de Century Weekly explique, par conséquent, que les campagnes chinoises constituent le point faible de la gestion des catastrophes naturelles. Selon les journalistes, seul un développement équilibré des villes et des campagnes permettrait de réduire les dégâts de façon significative.

#### Prévision du séisme
Ya’an est habituée aux séismes : depuis 1160, plus de 25 séismes destructeurs ont été enregistrés dans cette zone à haut risque sismique, appelée la « faille de Longmenshan » (龙门山地震断裂带, longmenshan dizhen duanliedai). Malgré ce lourd passé et malgré les recommandations des experts, les journalistes estiment que le gouvernement n'a pas su prendre la mesure du risque sismique dans cette région.

Depuis 2008, beaucoup de géologues ont en effet montré que cette faille comportait un risque sismique important. Shi Yaolin, expert en géophysique à l'académie chinoise des sciences sociales, estime donc que « le séisme de 2013 n’était pas un accident surprenant ». Plusieurs articles font aussi état d'une controverse scientifique portant sur les rapports entre les deux séismes. Chen Yuntai, directeur du Bureau chinois de sismologie, fait part de son étonnement : « Si le séisme de Lushan est une réplique de celui de Wenchuan, alors pourquoi le gouvernement n'a-t-il pas mis en place des analyses et des contrôles détaillés après 2008 ? ».

Après le grand séisme de 2008, des chercheurs chinois ont notamment appelé à la mise en place d'un système d’alerte précoce permettant de prévenir la population plusieurs minutes avant un séisme. Déjà présent au Japon, ce système est considéré comme pouvant réduire considérablement les dégâts humains et matériels. Cependant, en raison d’incertitudes chez les responsables chinois et du coût important du projet, il n'est resté pour l'instant qu'à un stade d'expérimentation. Certains experts chinois estiment en outre qu'une mise en place trop rapide, dans un pays où l’éducation et la prévention sont très faibles, pourrait générer un climat de panique et alourdir les dégâts.

En revanche, selon l'article de Century Weekly, un organisme privé, L'institut pour la prévention des catastrophes de Chengdu (成都高新减灾研究所, chengdu gaoxin jianzai yanjiusuo), a mis sur pied un système équivalent dont la zone de couverture s'étend sur une partie des provinces du Sichuan, du Shaanxi, du Gansu et du Yunnan, soit 200 000 km². Cet organisme, explique les reporters, a diffusé des informations actualisées sur le séisme quelques minutes puis quelques secondes avant les premiers tremblements. Cependant, en raison de lacunes du réseau télécom et de logiciels, cette alerte n'a pu parvenir qu'à 3 000 personnes. Les journalistes insistent donc sur la nécessité de revoir le système et de le diffuser à l'échelle régionale.

#### Mobilisation populaire
Le séisme de Ya’an a mis en évidence la mobilisation très importante de la société civile, tant dans la collecte de fonds que dans l'organisation des secours.

Les ONG sont des acteurs relativement nouveaux en Chine et avaient jusqu'à présent une capacité d'action limitée. Selon les journalistes de Caixin, depuis la grande inondation de 1998<sup>[1](#myfootnote1)</sup>, seules les organisations gérées par l’Etat (connues sous l'acronyme GONGO : « government organized non-gouvermental organization ») étaient agréées pour recevoir les dons du public et les utiliser. Lors du séisme de 2013, pour la première fois, le gouvernement n’a pas restreint le nombre d’organismes habilités à recevoir des dons et n’a pas obligé les ONG à reverser les fonds qu'elles avaient récolté. Les départements des affaires civiles de chaque niveau administratif se sont contentés d’orienter et de superviser les actions de secours.

De fait, les ONG ont même eu plus de succès que les GONGO. Deng Guosheng, directeur du Centre de recherche sur les ONG de l'Université Tsinghua, explique ainsi que de nombreux donateurs préfèrent se tourner vers les canaux non-gouvernementaux depuis le scandale Guo Meimei en 2010<sup>[2](#myfootnote2)</sup>, qui a grandement endommagé la crédibilité de la Croix-Rouge chinoise. En termes d’efficacité des secours, Wang Zhenyao, directeur de l'institut de recherche chinois sur la philanthropie, estime que les ONG ont agi de manière « plus rapide, plus professionnelle et plus raisonnable » que lors du séisme de 2008.

Cependant, la mobilisation importante de la société civile pose le problème de son encadrement et de sa transparence. Selon Wang Zhenyao, les exigences de transparence de l'opinion publique chinoise sont beaucoup plus élevées qu'en 2008, envers les ONG comme envers les GONGO. En conséquence, les deux types d’organisation ont fait de grands efforts pour accroître la transparence de leur financement et de leurs actions. La création d’un comité de surveillance chargé d'auditer les organisations a permis d'améliorer considérablement leur gestion.

En définitive, le séisme de 2013 a mis en évidence un progrès significatif dans la l’efficacité des secours et une importance accrue des organisations de la société civile. La prévention en amont, la mise en place de normes de construction antisismique dans les campagnes et l’accessibilité des zones sinistrées restent les points faibles de la gestion des catastrophes naturelles.

<a name="myfootnote1">1</a>: Les crues du Yangzi, notamment dans le Jiangxi, Hunan et Hubei, ont tué en 1998 plus de 3 000 personnes et causé une perte économique estimée à 24 milliards de dollars.

<a name="myfootnote2">2</a>: Guo Meimei avait fait scandale en se faisant passer comme la « Directrice générale de la Croix Rouge Commerciale Chinoise » sur Weibo et en exhibant des signes d’une extrême richesse.

*This article was first written for the China Analytica, which is the copyright holder.*
