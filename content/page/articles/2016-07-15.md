---
title: Un village chrétien en Chine
date: 2016-07-15
comments: false
paginate: true
showdate: true
tags: ["religion"]
categories: ["francais"]
description: "le Henan est la plus grande province chrétienne de Chine, et l’une des plus grandes régions chrétiennes d’Asie de l’est, avec plusieurs millions de croyants."
featuredImage : "/kaifeng.jpg"

---
<img src="/kaifeng.jpg" width="50%"/>
<div id="légende">Church in Kaifeng, Henan</div>


W.Y. est un village typique des plaines de Chine du nord. Situé dans le Henan, cette petite localité d’environ 2000 habitants est très pauvre et essentiellement agricole. Les maisons de briques rouges, parfois peintes en blanc ou recouvertes de plâtre sont ceintes d’un grand mur et flanquées d’une porte qui ouvre sur une cour intérieure.

Un fait étonne pourtant le passant curieux. S’il s’approche un peu, il s’apercevra que les murs de chaque maison sont décorés de crucifix et de calendriers religieux. Deux églises blanches, fondues dans l’architecture locale, mais s’en distinguant par une grande croix à l’entrée, attirent une foule de villageois le dimanche matin.

W. Y., en réalité, est loin d’être une exception. Surnommée la "Galilée de Chine", le Henan est la plus grande province chrétienne de Chine, et l’une des plus grandes régions chrétiennes d’Asie de l’est, avec plusieurs millions de croyants.

#### Débuts du christianisme
En 1907, le Cardinal Joly tentait d’expliquer, dans un livre à succès, Le Christianisme et l’Extrême-Orient, pourquoi l’évangélisation de la Chine avait échoué. Il est vrai que la situation était alors plutôt sombre pour le christianisme en Chine.

Après une phase d’expansion au 17e et 18e siècles, suivant l’arrivée en Chine du jésuite Matteo Ricci en 1682, la papauté avait refroidi les ardeurs de l’évangélisation jésuite, la jugeant trop prompte à adapter le catholicisme aux conditions locales. Au début du 18e siècle, l’empereur Yongzheng, à son tour, portait un coup au travail d’évangélisation en interdisant le christianisme en Chine.

Au 19e siècle, malgré l’arrivée massive de missionnaires et l’ouverture forcée de la Chine à l’occident, l’évangélisation du pays était un échec évident, et l’attitude méprisante des missionnaires envers le clergé et la population locale provoquaient des tensions qui allaient affecter durablement l’expansion du christianisme en Chine.<sup>[1](#myfootnote1)</sup>

Un siècle plus tard en revanche, la question semble être inversée. Aujourd’hui, selon des sources officielles chinoises, il y aurait aujourd’hui 29 millions de chrétiens en Chine, soit environ 2% de la population. Mais la plupart des sources donnent des chiffres allant de 50 à 80 millions<sup>[2](#myfootnote2)</sup>, dont neuf millions de catholiques, et une grande majorité sont ruraux et non enregistrés dans des églises officielles.

Cette montée spectaculaire du christianisme dans la Chine post-maoïste a de quoi étonner. Deux principales explications ont été avancées : l’admiration pour les puissants pays occidentaux à une époque de libéralisation et d’ouverture au monde de la Chine, et le besoin spirituel grandissant d’une population confrontée au matérialisme et au capitalisme à outrance, est difficile à évaluer.
Mais l’explication principale est à trouver dans le contexte politique particulier des années 70 et 80.  Le chaos de la révolution culturelle a favorisé l’émergence de formes non institutionnalisées de religion<sup>[3](#myfootnote3)</sup>, tandis que la répression du christianisme dans les années 70 semble avoir eu des conséquences tout à fait contraires à son intention initiale, en créant un climat de crise dans lequel les villageois devaient lutter pour leurs croyances<sup>[4](#myfootnote4)</sup>.

<img src="/religion.jpg" width="50%"/>
<div id="légende">"Distribution of Christians in China in the 2010s, by province, according to China General Social Survey 2009 (author Aethelwolf Emsworth)".</div>

### Diversité des pratiques
Ces complexités historiques ont donné un tableau très constaté de la Chine d’aujourd’hui : entre les protestants urbains affiliés à des congrégations dans lesquelles la religion est souvent l’affirmation d’un statut social, les protestants ruraux des ‘églises de maison’ qui s’apparentent bien souvent à des sectes ; ou encore les catholiques de ces villages convertis au 17e siècle, dont la pratique s’est cimentée et internationalisée au cours des siècles.

Reflet de cette diversité religieuse, W.Y. compte deux églises appartenant à des mouvances différentes. La première est affiliée à l’organisation officielle, le "Mouvement des Trois Autonomies" (MTA). Fondée en 1950, après que Mao a pris le pouvoir en Chine et en a expulsé les missionnaires étrangers, elle compte aujourd’hui 10 millions de croyants.
La deuxième appartient à l’une des nombreuses ‘églises de maisons’, qui sont les principales bénéficiaires du grand élan religieux que connaît la Chine actuellement. Des raisons historiques peuvent expliquer cet engouement. En particulier, le mouvement Pentecôtiste, né en Californie dans les années 1900, a rencontré beaucoup d’influence en Chine dès le début du 20e siècle en raison du rejet des missionnaires occidentaux<sup>[5](#myfootnote5)</sup>.

Son interprétation littérale de la bible et son accent sur l’inspiration divine, les croyances apocalyptiques prédisant la fin du monde imminente, et la capacité du Saint Esprit à donner de nouveaux pouvoirs aux croyants, donnaient plus d’importance aux prêtres chinois et favorisaient un christianisme ‘populaire’, plus proche des mouvement religieux chinois.
Le catholicisme chinois n’a pas connu la même croissance que les églises protestantes. En 1949, on comptait en Chine 3 millions de catholiques, contre un million de protestants<sup>[6](#myfootnote6)</sup>.

Aujourd’hui, le catholicisme ne représente plus que 9 millions<sup>[7](#myfootnote7)</sup>, soit une petite portion de la population chrétienne chinoise. Très proche des coutumes folkloriques locales<sup>[8](#myfootnote8)</sup>, le catholicisme chinois, surtout présent au Hebei et dans le nord de la chine, est également teinté de pratiques religieuses étonnamment similaires à celles du sud de l’Italie, dont étaient originaires les missionnaires franciscains : des pèlerinages de village, des litanies et rosaires, des rituels élaborés pour aider l’âme d’un parent à atteindre le paradis, ou encore des transes visionnaires. Dans ces villages où le catholicisme s’est cristallisé depuis le 17e siècle, le progrès des technologies de transport et de communication ont cependant rapproché les pratiques religieuses des standards internationaux, et les croyants se sentent de plus en plus liés à une communauté religieuse internationale<sup>[9](#myfootnote9)</sup>.

Cette pluralité religieuse, cependant, n’a qu’une existence précaire, car la plupart de ces églises non officielles sont en réalité illégales. Souvent tolérées, elles sont parfois réprimées, et il semblerait qu’elles ne soient pas à l’abri de la vague répressive qui caractérise l’administration Xi Jinping. Lors d’une conférence nationale sur la religion les 22 et 23 avril 2016 à Pékin, le président chinois a en effet appelé les leaders religieux à réaffirmer le contrôle du Parti Communiste Chinois sur la vie religieuse<sup>[10](#myfootnote10)</sup>.

Cet effort de contrôle sur les églises non officielles est d’autant plus important aujourd’hui que l’arrivée de millions de migrants ruraux dans les grandes villes pose de manière pressante la question de l’intégration de croyants aux origines et aux pratiques très différentes. Mais un tel contrôle semble délicat, alors que c’est précisément la diversité et la fragmentation des églises, notamment protestantes, qui leur a permis d’échapper à toute institutionnalisation<sup>[11](#myfootnote11)</sup>.

<a name="myfootnote1">1</a>: Harrison, H. (2013). The Missionary's Curse and Other Tales from a Chinese Catholic Village (Vol. 26). Univ of California Press.

<a name="myfootnote2">2</a>: According to the Bertelsmann Stiftung Transformation Index (BTI), which analyzes the quality of democracy and political management in 128 countries (Bertelsmann Stiftung n.d.), there are an estimated 80 million Christians in China, "many of whom congregate in illegal house churches" (ibid. 2014, 5)

<a name="myfootnote3">3</a>: Kao, C. Y. (2009). The Cultural Revolution and the emergence of Pentecostal-style Protestantism in China. Journal of Contemporary Religion, 24(2), 171-188.

<a name="myfootnote4">4</a>: Harrison, H., ibid.

<a name="myfootnote5">5</a>:Lim, F. K. G. (Ed.). (2013). Christianity in contemporary China: socio-cultural perspectives (Vol. 5). Routledge.

<a name="myfootnote6">6</a>: cf https://www.calvin.edu/nagel/resources/files/bays_chinese_protestant.pdf

<a name="myfootnote7">7</a>: The Pew Center estimates in 2011 there are nine million Catholics on the mainland, 5.7 million of whom are affiliated with the CPA.

<a name="myfootnote8">8</a>: See Richard P. Madsen, “Beyond orthodoxy: Catholicism as Chinese folk religion.”

<a name="myfootnote9">9</a>: Harrison, H. ibid.

<a name="myfootnote10">10</a>: cf http://news.xinhuanet.com/english/2016-04/23/c_135306131.htm

<a name="myfootnote11">11</a>: Ryan Dunch, “Protestant Christianity in China today: fragile, fragmented, flourishing,” in Stephen Uhalley, Jr. and Xiaoxin Wu (eds.), China and Christianity: Burdened Past, Hopeful Future (Armonk, NY: M.E. Sharpe, 2001), pp. 195–216.
