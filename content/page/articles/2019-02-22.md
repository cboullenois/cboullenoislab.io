---
title: Anti-monopoly enforcement will strengthen in China
date: 2019-02-22
publishdate: 2019-05-22
comments: false
showdate: true
paginate: true
tags: ["economy","competition"]
categories: ["briefs"]
description: "Beijing has consolidated its anti-monopoly authorities within a single organisation."
featuredImage : "/competition.jpg"
---
<img src="/competition.jpg" width="50%"/>

> Beijing has consolidated its anti-monopoly authorities within a single organisation

Beijing late last year consolidated its anti-monopoly authorities within a single organisation, the State Administration for Market Regulation.

### What next
This overhaul indicates the intention to step up the enforcement of a decade-old anti-monopoly framework. The unified market regulator will have enhanced supervision capacities, better information- sharing mechanisms and a more coherent approach.

### Subsidiary Impacts
- The internet economy will be a major focus of future legislation and anti-monopoly action.
- Regulators will seek a balance between protecting intellectual property and preventing foreign companies charging excessive licensing fees.
- Anti-monopoly regulations may be used more selectively and aggressively if economic tensions with the United States are not de-escalated.

### Analysis
Consolidation of anti-monopoly powers within the State Administration for Market Regulation (SAMR) was completed in September 2018 as part of a sweeping reform of governmental structures (see <a class="article-more-link" href="/page/articles/2018-04-06">Xi's big bureaucratic shake-up</a>). The SAMR deals with cases that were previously undertaken by three separate authorities:

- price-related market dominance abuses (National Development and Reform Commission);
- non-price-related market dominance abuses (State Administration of Industry and Commerce);
- merger reviews (Ministry of Commerce).

The overhaul aims to allow regulators to share information better, make implementation more uniform, better supervise the implementation of remedies and take an even more active role in worldwide antitrust activity.

> A number of recent and upcoming guidelines and legislation will further enhance the SAMR's role

A review of the Anti-Monopoly Law was added to the legislative schedule of China's 13th National People's Congress, and is expected to be enacted in the next five years.

Last month, the regulation of monopolies and abuses of market dominance within a given geographical region was delegated to provincial-level market supervision departments, under the monitoring of the SAMR, to increase manpower and improve local practice.

#### The Anti-Monopoly Law

##### Mergers

The regulator is required to consider the impact of mergers on market share, but also on consumers and on national economic development.

Compared with the United States and the EU, China has made more active use of 'behavioural remedies', such as commitments to maintain fair prices and supply volume. The United States and EU prefer 'structural remedies' (divestiture of assets), which minimises government intervention in the market. China is also distinct in imposing 'hold separate' remedies, which require companies to keep certain operations independent after the merger.

Chinese regulators investigated 2,437 merger cases between 2008 (when the law was introduced) and October 2018, prohibiting two transactions and imposing remedies on 37. The number of mergers reviewed has risen over the years to a level similar to that in the EU but still below the US Federal Trade Commission.

##### Monopoly agreements

The Anti-Monopoly Law prohibits cartels. Agreements to allocate market share, limit output or fix prices are considered illegal unless it can be proven that they promote the public interest, for example by improving technology or protecting the environment.

Chinese regulators investigated 165 cartel cases between 2008 and October 2018, a high number compared to their EU and US counterparts, but they imposed much lower fines than the EU.

##### Abuses of market position

The Anti-Monopoly Law prohibits what it deems "abuse of dominant market position", such as selling commodities at unfairly high or low prices. Several factors are considered, including market share and the ability to control the raw materials procurement market.

High penalties have been imposed for abuse of dominant market position, with a record-high fine of 6.08 billion renminbi (904 million dollars) on Qualcomm in 2015.

Overall, Chinese regulators have been more active than their US counterparts in investigating abuse of dominance, with 55 cases between 2008 and October 2018.

Chinese regulators also tend to regulate price-related conduct more strictly than their Western counterparts.

#### The internet and 'unfair competition'

China's Law Against Unfair Competition focuses on fraud, false advertising, stealing trade secrets and other deceptive business practices. Passed in 1993, it was revised in 2017, eliminating overlaps with the Anti-Monopoly Law and strengthening the regulator's supervisory and investigative powers.

The revised Law Against Unfair Competition includes an article prohibiting new types of conduct by internet companies, such as undermining the compatibility of network products offered by other companies.

Chinese courts have also considered, in a landmark 2016 judgement (Hantao v. Baidu), that unauthorised use of consumer reviews grabbed from a website was illegal under this law.

New guidelines will be issued to complement the framework and monitor the internet economy more effectively. The 'sharing economy' will also attract regulatory attention, with a decision still pending on the merger between Uber's China operations and local ride-sharing company Didi.

#### Intellectual property

A major motive for passing the Anti-Monopoly Law was concern among officials that foreign companies had gained a dominant position and abused their monopoly of intellectual property rights (IPR). IPR have become a key area in China's anti-monopoly enforcement, covering up to 34% of current cases. In the 2015 Qualcomm case, for example, regulators found that the company was charging unfairly high price when licensing 'standard essential patents' (SEPs).

As China becomes a global leader in intellectual property, regulators may be more reluctant to enforce harsh IPR-related anti-monopoly action.

A State Council Anti-Monopoly Committee draft guideline for comments entitled 'Anti-monopoly guide on abuse of intellectual property rights', first released in 2015, illustrates the conundrum. It states that IPR are within the scope of Anti-Monopoly Law but creates a safe harbour for IP companies below certain thresholds. The draft guideline was not enacted on account of the controversies surrounding this issue.

#### Limitations

In principle the Anti-Monopoly Law prohibits state-owned enterprises (SOEs) from creating monopolies and abusing their dominant position, but it contains exemptions for national security and industries that are deemed vital to the national economy.

> Anti-monopoly enforcement regarding state-owned enterprises has been disproportionately low

When SOEs were investigated, penalties have generally been very low or replaced by rectification, and illegal gains not confiscated. In 2018, several SOEs in power generation, shipping, petrochemical, natural gas and ports were given relatively low fines.

The Anti-Monopoly Law also prohibits 'administrative monopolies' -- state administrations abusing their power to restrict private transactions. However, although a number of administrative monopoly cases have been investigated, only third parties were given penalties.

The 19th Party Congress has called for breaking administrative monopolies, but central and local governments are unlikely to make significant progress, due to entrenched interests and the lack of structural reform.

Another weakness is the gap between judicial and administrative practice. Regulators have often imposed penalties on companies, but courts have only ruled in favour of a handful of plaintiffs in the more than 700 civil suits involving the Anti-Monopoly Law. This is because plaintiffs bear the burden of presenting evidence of anti-competitive effect, while administrative authorities are not required to prove such effect.

#### Trade war weapon?

Many of the highest fines have been imposed on foreign companies. In 2014, the European Chamber of Commerce complained of abuse of power and intimidation tactics from Chinese anti-monopoly regulators.

Now, with tensions high between China and the United States, anti-monopoly regulations could be used to crack down, prevent mergers or impose heavy fines on US companies.

When US chipmaker Qualcomm last year abandoned a 44-dollar bid to acquire NXP Semiconductors because of repeated delays from Chinese regulators, this may have been a cautionary message.

*This article was first written for the Oxford Analytica Daily Brief, which is the copyright holder.*
