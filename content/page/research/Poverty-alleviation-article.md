---
title: "Poverty Alleviation in China: The Rise of State-Sponsored Corporate Paternalism"
date: 2020-10-02
comments: false
paginate: false
showdate: true

---
<img src="/taiqian/fupin.jpg" width="50%"/>

My recent article about poverty alleviation policies in China, published by China Perspectives, is available  <a class="article-more-link" href="https://journals.openedition.org/chinaperspectives/10456">here</a>.

While the full text will be posted only on September 2021 because of copyright issues, here is an abstract of the paper:

Since taking office, president Xi Jinping’s government has granted massive funding to what has become China’s strongest poverty-reduction campaign ever.

Based on the study of detailed budgets in eight rural counties, as well as ethnographic and interview data in a ninth county, this article explores how poverty alleviation programs shape the distribution of power and resources in rural China.

It argues that poverty alleviation in rural China predominately focuses on infrastructure investment and support to the local economy, rather than on social insurance, education, and household subsidies. Support to local companies, the article argues, entails co-opting established enterprises, rather than supporting new entrepreneurship among poor households. Overall, the Chinese approach to rural poverty alleviation highlights the emergence of a state-sponsored corporate paternalism that strengthens local hierarchies of wealth and power.
