---
title: "The self-made entrepreneur: Social identities and the perceived legitimacy of entrepreneurs in inland rural China"
date: 2020-09-15
comments: false
paginate: false
showdate: true

---
<img src="/taiqian/Taiqian4bis.png" width="30%"/>

I am about to submit my PhD thesis about "The self-made entrepreneur: Social identities and the perceived legitimacy of entrepreneurs in inland rural China". I will post the full text after submission, but here is an abstract of what I investigate in my thesis.

This PhD thesis draws on a case study of entrepreneurship in contemporary rural China to address questions relating to social mobility, social differentiation, perceptions of social fairness and legitimacy, and the formation of collective identities in a context of rapid socio-economic change.

Since 1978, liberal economic reforms initiated by the Chinese government have established the conditions for tremendous socio-economic change across inland rural China, including sudden social mobility, a disruption of social hierarchies and swift changes in social perceptions of inequality and fairness. Entrepreneurs have been at the core of these shifts. Although private entrepreneurs in China struggled against public prejudice during the first decades of the post-Mao period, they have enjoyed much more positive social recognition since 2000, in parallel with their economic success.

Against this backdrop, the thesis addresses two major research questions. Firstly, how and why has the rise of entrepreneurs as a privileged group in the rural social hierarchy been perceived as legitimate and fair by the local population in the post-Mao period, despite decades of Maoist rule, during which market activities and the pursuit of individual profit were severely discouraged? Secondly, when and how did rural entrepreneurs begin to identify as a social group distinct from the rest of the local population?

To answer these questions, the thesis combines analysis of quantitative survey data, policy analysis, and ethnographic fieldwork in X County – a rural county in Henan province. Along with interviews with more than fifty entrepreneurs in X county, the author immersed herself in an ethnographic study by working in a rural factory for several weeks, and by following entrepreneurs in their daily business activities. 
The thesis argues that a narrative of the “self-made entrepreneur”, with its understanding that any individual can, with hard work, perseverance, and sound judgment, move up the social ladder, has become central to perceptions of social mobility and social fairness in post-Mao rural China. This narrative has played a crucial role in justifying and legitimating entrepreneurs’ subsequent rise to a relatively high position in local economic and social hierarchies, even though the gap between that narrative and the reality of entrepreneurs’ achievement and maintenance of wealth and social status has widened. The self-made entrepreneur narrative thus helps explain why the "social volcano" of discontent and conflict feared by Chinese observers and leaders has remained mostly inactive, at least when it comes to the position of private entrepreneurs in the Chinese social hierarchy.

In addition, the thesis shows that only the second generation of rural entrepreneurs, who set up businesses in the first two decades of the 21st century, have begun to identify as a social group that is clearly distinct from the rest of the local population. The thesis identifies intentional depersonalisation of social relationships as a key mechanism shaping the growing differentiation between social identities as bosses and workers. This growing social differentiation parallels shifts in the geographical basis of identity, as rural entrepreneurs start to identify more with a nationally defined social community.
