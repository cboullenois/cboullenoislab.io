---
title: "Fieldwork in Henan: a few pictures"
comments: false
paginate: false
showdate: false
draft : true
---

Here are a few pictures of my fieldwork in Henan.

During the last months of fieldwork I have attended several weddings and funerals. Here are a few pictures from there.

During the funeral, the descendants of the deceased wore white hats made of paper and paid their respects by praying in the family’s courtyard. One member of each family in the village came and offered money to the family of the deceased. Then a funeral procession, led by a band of musicians, brought the coffin to the burial site. There was no cemetery: the deceased was buried in the family land.

<img src="/taiqian/funeral1.jpg" width="50%">

<img src="/taiqian/funeral2.jpg" width="50%">

<img src="/taiqian/funeral3.jpg" width="50%">

<img src="/taiqian/funeral4.png" class="rotate" width="60%" >

Most of the weddings there take place around the Chinese New Year when the migrants come home. This wedding started early in the morning with both families gathering at the groom’s home. Following the Chinese nuptial tradition, the groom was teased by his friends and family who tied him to a pole and threw eggs at him. After the ceremony, during which the bride called her parents-in-law ‘mum’ and ‘dad’, both families gathered for lunch in a nearby restaurant.

<img src="/taiqian/wedding1.jpg" width="50%">

<img src="/taiqian/wedding2.jpg" width="50%">

<img src="/taiqian/wedding3.jpg" width="50%">

<img src="/taiqian/wedding4.jpg" width="50%" class = "rotate">

Here are a few pictures from daily life in rural Henan. This one is from the two children of dear friends of mine.

<img src="/taiqian/others1.jpg" width="50%">

This is market day in a rural township.

<img src="/taiqian/others2.jpg" width="50%" class = "rotate">

The local (Christian) church in a small 2000 people village.

<img src="/taiqian/others3.jpg" width="50%" class = "rotate">

State advertising for returning migrants and entrepreneurship: "go back to the countryside and start a business!"

<img src="/taiqian/others4.jpg" width="50%" class = "rotate">

And this is Chinese New year in a village.

<img src="/taiqian/others5.jpg" width="50%" class = "rotate">

And last, fieldwork in China always involve quite a lot of drinking!

<img src="/taiqian/drinking1.jpg" width="50%">

<img src="/taiqian/drinking2.jpg" width="50%">
