---
title: "China's Cultural Revolution under the French Eyes: Diplomats and Travellers in China from 1965 to 1971"
date: 2011-09-15
comments: false
paginate: false
showdate: true
#draft : true

---
<img src="/cultural_revolution.jpg" width="40%"/>

This master's thesis, based on a research conducted in 2010-2011, investigates China's Cultural Revolution under the French Eyes.

The events of this movement were kept at a distance from Westerners who could scarcely grasp the political upheavals that unleashed a decade of violence and chaos across the country.

In this context, French Sinologists, diplomats, experts and journalists, who lived and traveled throughout the country, struggled to decipher the political life of revolutionary China.

Based on an in-depth exploration of the diplomatic correspondence of the French Embassy in Beijing from 1964 to 1971, as well as books, press articles, and interviews with witnesses and French residents in Beijing, my study investigated the living conditions in China, and the possibilities for observation and circulation, in a period when most of the Chinese territory was closed to foreigners, and the political surveillance was omnipresent.

It also examines the imaginary and the interpretative frame that served as reference for French observers to make sense of China’s cultural revolution, highlighting the ideologies, concepts and paradigms through which China was understood abroad.

You can find my thesis in French <a class="article-more-link" href="/Memoire2.pdf">here</a>.
