---
title: "Social Changes and Power Relations in Picun Village"
date: 2013-09-15
comments: false
paginate: false
showdate: true

---
<img src="/picun14.jpg" width="50%"/>

This master's thesis, based on a research conducted in 2012-2013, analyses the appropriation of residential space in places of cohabitation between local populations and rural migrants, specifically the courtyard houses (siheyuan, 四合院   ) in Beijing’s periphery, and the impact this phenomenon has on relationships between these groups.

Interviews and observations from a field survey conducted in 2013 in the village of Picun, northwest of Chaoyang District, form the basis of this survey. In this village, located at the interface between the city and the countryside, the influx of rural migrants caused real estate pressure and created an economic dynamism that led to a change in social relationships. Local villagers have gradually abandoned agriculture and rebuilt their homes so that they can rent rooms to migrants, bringing about mixed forms of residential space which combine traditional courtyards and multi-story buildings.

This research highlights how institutional inequalities created by the hukou system still shape the relationships between local people and migrant workers, providing local villagers with higher positions in the authority structure, control of the land property and of the police force, and exclusive legal right to live in the village permanently.

However, this research shows that migrants can’t be considered as a homogeneous social group. It points out the emergence, within the migrants, of a capitalistic class of small business owners, their relationship with the workers being both paternalistic and highly conflictual.

Last, it highlights how the new spatial configuration, blurring the boundaries between private and public places, and resulting in cohabitation between local villagers and migrants, has led to the emergence of specific new relationships. Through daily interaction, the phenomenon of ongoing "social categorisation" is developing, which confers a particular importance to the opposite pairs of landlord/tenant and local/ migrant.

You can find my thesis in French <a class="article-more-link" href="/Memoire1.pdf">here</a>.
